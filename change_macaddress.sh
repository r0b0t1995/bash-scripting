#!/bin/bash

u=$(whoami)

if [ $u = "root" ]; then

	old_macaddress=$(cat /sys/class/net/*/address)

	command ifconfig eth0 down

	sleep 1

	command ifconfig eth0 hw ether 00:99:00:99:00:99

	sleep 1

	command ifconfig eth0 up

	new_macaddress=$(cat /sys/class/net/*/address)

	echo "Old MAC ADDRESS: ${old_macaddress::17}"

	echo "New MAC ADDRESS: ${new_macaddress::17}"
else
	echo "You need to use the root user"
fi

