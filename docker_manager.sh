#!/bin/bash

user=$(whoami)

if [ $user = "root" ]; then

	echo "[1] -> start containers"

	echo "[2] -> stop containers"

	read v

	if [ $v -eq 1 ]; then
		v_command="start"
		sms="docker start containers"
	else
		v_command="stop"
		sms="docker stop containers"
	fi

	docker_IDs=$(docker ps -aq)

	command docker $v_command $docker_IDs

	echo $sms

else 
	echo  "You need to user the root user"
fi

